from django.contrib import admin
from .models import TarjetaCredito, Arriendo, Bicicleta, Viaje

admin.site.register(TarjetaCredito)
admin.site.register(Arriendo)
admin.site.register(Bicicleta)
admin.site.register(Viaje)

