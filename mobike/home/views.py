from django.shortcuts import render, redirect
from django.http.response import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required, permission_required
from .forms import TarjetaCreditoForm, CodigoActivacionForm
from .models import TarjetaCredito, Bicicleta, Viaje, Arriendo
from django.utils.crypto import get_random_string
# Create your views here.

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def index(request):

    bicicletas = Bicicleta.objects.all()
    request.session['arriendo'] = None
    request.session['bicicleta'] = None
    request.session['tarjeta'] = None

    return render(request,'home/index.html',{'bicicletas':bicicletas})

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def perfil(request):
    pass

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def agregarTarjetaCredito(request):

    form_tarjeta = TarjetaCreditoForm()

    if request.method == 'POST':

        form_tarjeta = TarjetaCreditoForm(data=request.POST)

        print(form_tarjeta.is_valid())
        
        if form_tarjeta.is_valid():

            tipo = form_tarjeta.data['tipo_tarjeta']
            duenio = request.user
            numero = form_tarjeta.cleaned_data['numero']
            fecha_vencimiento = form_tarjeta.cleaned_data['expiration']
            cvv = form_tarjeta.cleaned_data['cvv']
            nombre = form_tarjeta.cleaned_data['nombre']

            tarjeta = TarjetaCredito()
            tarjeta.dueno = duenio
            tarjeta.numero = numero
            tarjeta.fecha_vencimiento = fecha_vencimiento
            tarjeta.cvv = cvv
            tarjeta.nombre = nombre
            tarjeta.tipo = tipo

            tarjeta.save()
        
            return redirect('home:index')

    return render(request, 'home/formulario_tarjeta.html', {'form': form_tarjeta})

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def seleccionarArriendo(request, pk):

    lista_arriendos = Arriendo.objects.all()
    bici = Bicicleta.objects.get(id=pk)
    request.session['bicicleta'] = bici.id

    return render(request, 'home/seleccionar_arriendo.html', {'arriendo':lista_arriendos})

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def seleccionarFormaDePago(request, pk):

    tarjeta = TarjetaCredito.objects.filter(dueno_id=request.user)
    arriendo = Arriendo.objects.get(id=pk)
    request.session['arriendo'] = arriendo.id

    return render(request, 'home/seleccionar_tarjeta.html', {'tarjeta':tarjeta})

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def resumenViaje(request, pk):

    tarjeta = TarjetaCredito.objects.get(id=pk)
    arriendo = Arriendo.objects.get(id=request.session['arriendo'])
    bicicleta = Bicicleta.objects.get(id=request.session['bicicleta'])

    return render(request, 'home/resumen.html', {'tarjeta':tarjeta,'arriendo':arriendo,'bicicleta':bicicleta})

@login_required(login_url='mantenedor_usuario:inicio_sesion')
def generarCodigo(request, pk):

    codigo_activacion = get_random_string(length=5)
    bicicleta = Bicicleta.objects.get(id=pk)
    bicicleta.codigo = codigo_activacion
    bicicleta.save()

    return render(request, 'home/codigo.html', {'cod':codigo_activacion})

def desbloqueoBicicleta(request, pk):

    bici = Bicicleta.objects.get(id=pk)
    errores = False

    if request.method == 'POST':

        codigo_form = CodigoActivacionForm(data=request.POST)

        if codigo_form.is_valid():

            codigo = codigo_form.data['codigo']

            if codigo == bici.codigo:

                bici.estado = True
                bici.codigo = ''
                bici.save()

                return redirect('/home/')
            
            else:

                errores = True
    else:

        codigo_form = CodigoActivacionForm()

    return render(request, 'home/codigo_activacion.html', {'codigo_form': codigo_form,'error':errores})

def bloquearBicicleta(request, pk):

    bicicleta = Bicicleta.objects.get(id=pk)
    bicicleta.estado = False
    bicicleta.save()

    return redirect('/')









