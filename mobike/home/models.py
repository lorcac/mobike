from django.db import models
from mantenedor_usuario.models import Usuario


class TarjetaCredito(models.Model):

    dueno = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    numero = models.IntegerField()
    nombre = models.CharField(max_length=200)
    fecha_vencimiento = models.DateField()
    cvv = models.IntegerField()
    tipo = models.CharField(max_length=200)

    objects = models.Manager()

class Bicicleta(models.Model):

    fecha_registro = models.DateField()
    estado = models.BooleanField(default=False)
    codigo = models.CharField(max_length=5, blank=True, null=True)

    objects = models.Manager()

class Viaje(models.Model):

    hora_inicio = models.DateTimeField()
    hora_fin = models.DateTimeField()
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    bicicleta = models.ForeignKey(Bicicleta, on_delete=models.CASCADE)

    objects = models.Manager()

class Arriendo(models.Model):

    descripcion = models.CharField(max_length=200)
    precio = models.IntegerField()

    objects = models.Manager()




