from django.contrib import admin
from django.urls import path
from . import views

app_name='home'

urlpatterns = [

    path('', views.index, name='index'),
    path('perfil/', views.perfil, name='perfil'),
    path('agregar_tarjeta/', views.agregarTarjetaCredito, name='agregar_tarjeta'),
    path('seleccionar_arriendo/<str:pk>/', views.seleccionarArriendo, name='seleccionar_arriendo'),
    path('seleccionar_forma_de_pago/<str:pk>/', views.seleccionarFormaDePago, name='seleccionar_forma_de_pago'),
    path('resumen/<str:pk>', views.resumenViaje, name='resumen'),
    path('pago_finalizado/<str:pk>/', views.generarCodigo, name='pago_finalizado'),
    path('desbloquear/<str:pk>/', views.desbloqueoBicicleta, name='desbloquear'),
    path('bloquear/<str:pk>/', views.bloquearBicicleta, name='bloquear'),
]