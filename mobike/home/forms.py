from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from .models import TarjetaCredito
from datetime import date, datetime
from calendar import monthrange
from django.core.validators import EMPTY_VALUES

class CCExpWidget(forms.MultiWidget):
    """ Widget containing two select boxes for selecting the month and year"""
    def decompress(self, value):
        return [value.month, value.year] if value else [None, None]

    def format_output(self, rendered_widgets):
        html = u' / '.join(rendered_widgets)
        return u'<span style="white-space: nowrap">%s</span>' % html


class CCExpField(forms.MultiValueField):
    EXP_MONTH = [(x, x) for x in range(1, 13)]
    EXP_YEAR = [(x, x) for x in range(date.today().year,
                                       date.today().year + 15)]
    default_error_messages = {
        'invalid_month': u'Introduzca un mes valido..',
        'invalid_year': u'Introduzca un año valido.',
    }

    def __init__(self, *args, **kwargs):
        errors = self.default_error_messages.copy()
        if 'error_messages' in kwargs:
            errors.update(kwargs['error_messages'])
        fields = (
            forms.ChoiceField(choices=self.EXP_MONTH,
                error_messages={'invalid': errors['invalid_month']}),
            forms.ChoiceField(choices=self.EXP_YEAR,
                error_messages={'invalid': errors['invalid_year']}),
        )
        super(CCExpField, self).__init__(fields, *args, **kwargs)
        self.widget = CCExpWidget(widgets =
            [fields[0].widget, fields[1].widget])

    def clean(self, value):
        exp = super(CCExpField, self).clean(value)
        if date.today() > exp:
            raise forms.ValidationError(
            "La fecha de expiracion ingresada es del pasado.")
        return exp

    def compress(self, data_list):
        if data_list:
            if data_list[1] in EMPTY_VALUES:
                error = self.error_messages['invalid_year']
                raise forms.ValidationError(error)
            if data_list[0] in EMPTY_VALUES:
                error = self.error_messages['invalid_month']
                raise forms.ValidationError(error)
            year = int(data_list[1])
            month = int(data_list[0])
            # find last day of the month
            day = monthrange(year, month)[1]
            return date(year, month, day)
        return None



class TarjetaCreditoForm(forms.ModelForm):

    expiration = CCExpField(required=True,label= "Fecha de Vencimiento")

    class Meta:

        model = TarjetaCredito
        fields = ('numero','nombre','cvv','expiration')
    
    def __init__(self, *args, **kwargs):

        super(TarjetaCreditoForm, self).__init__(*args,**kwargs)

        for field in self.fields:
            
            self.fields[field].widget.attrs.update({'class': 'form-control'})

class CodigoActivacionForm(forms.Form):

    codigo = forms.CharField(label='Codigo de Activación', max_length=5)

    def __init__(self, *args, **kwargs):

        super(CodigoActivacionForm, self).__init__(*args,**kwargs)

        for field in self.fields:
            
            self.fields[field].widget.attrs.update({'class': 'form-control'})



