from django.contrib import admin
from django.urls import path
from . import views

app_name='mantenedor_usuario'

urlpatterns = [
    path('', views.index, name='index'),
    path('registro/',views.register, name='registro'),
    path('inicio_sesion/',views.login, name='inicio_sesion'),
    path('cerrar_sesion/',views.logout, name='cerrar_sesion'),
    
]