from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):

    run = models.IntegerField(null=True)
    dv = models.CharField(max_length=1,null=True)

