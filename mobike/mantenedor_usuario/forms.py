from django import forms
from django.forms import widgets
from django.forms.models import fields_for_model
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import Usuario
from itertools import cycle

def validate_dv(run):
    reversed_digits = map(int, reversed(str(run)))
    factors = cycle(range(2, 8))
    s = sum(d * f for d, f in zip(reversed_digits, factors))
    return (-s) % 11

class RegistrationForm(UserCreationForm):

    class Meta:

        model = Usuario
        fields = ('username','first_name' ,'last_name' , 'email', 'password1', 'password2', 'run','dv') 

    def clean_dv(self):

        dv = self.cleaned_data.get('dv')

        run = self.cleaned_data.get('run')

        dv2 = validate_dv(run)

        print(dv2,dv)

        if str(dv) != str(dv2):

            raise forms.ValidationError('Ingrese un Rut valido.')
        
        return dv

    def __init__(self, *args, **kwargs):

        super(RegistrationForm, self).__init__(*args,**kwargs)

        for field in self.fields:
            
            self.fields[field].widget.attrs.update({'class': 'form-control'})

 
        for field in self.fields.values():
                field.help_text = None

class LoginForm(AuthenticationForm):

    def __init__(self, *args, **kwargs):

        super(LoginForm, self).__init__(*args,**kwargs)

        for field in self.fields:
            
            self.fields[field].widget.attrs.update({'class': 'form-control'})    