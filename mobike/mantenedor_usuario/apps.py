from django.apps import AppConfig


class MantenedorUsuarioConfig(AppConfig):
    name = 'mantenedor_usuario'
