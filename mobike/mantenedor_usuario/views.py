from django.shortcuts import render, redirect
from django.http.response import HttpResponse, HttpResponseRedirect
from .forms import RegistrationForm, LoginForm
from django.contrib.auth import login as log_in, logout as log_out
from .auth import AuthenticationEmailBackend
from django.contrib.auth import authenticate
from mantenedor_usuario.models import Usuario


def index(request):

    if not request.user.is_anonymous:
        return redirect('home:index')
    return render(request,'mantenedor_usuario/index.html',{})

def register(request):

    registration_form = RegistrationForm()

    if request.method == "POST":

        registration_form = RegistrationForm(data=request.POST)

        if registration_form.is_valid():

            user = registration_form.save()

            username = registration_form.cleaned_data['username']
            password = registration_form.cleaned_data['password1']

            user = authenticate(username=username,password=password)

            if user is not None:

                log_in(request, user)

                return redirect('mantenedor_usuario:index')


    return render(request, "mantenedor_usuario/register.html", {'registration_form': registration_form})

def login(request):

    login_form = LoginForm()

    if request.method == "POST":
        print('pasa')
        login_form = LoginForm(data=request.POST)

        print(login_form.is_valid())
        if login_form.is_valid():

            username = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:

                log_in(request, user)

                return redirect('home:index')

            user = AuthenticationEmailBackend(username=username, password=password)

            if user is not None:

                log_in(request, user)

                return redirect('home:index')

    return render(request, 'mantenedor_usuario/login.html',{'login_form':login_form})

def logout(request):

    log_out(request)

    return redirect('mantenedor_usuario:index')

